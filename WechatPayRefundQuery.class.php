<?php 
/**
 * @name 微信支付退款成功回调接口 
 * @author crazymus < QQ:291445576 >
 * @licenced apache2.0
 * @updated 2016-01-19
 */
 
namespace Org\Util;
interface WechatPayRefundQuery{
	/**
	 * @name 退款成功后需要完成的业务逻辑 
	 * @param 微信传递的支付信息，参见
	 https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_5
	 成功后的返回值  
	 * @return boolean 
	 */
	public function success($array);
}

?>