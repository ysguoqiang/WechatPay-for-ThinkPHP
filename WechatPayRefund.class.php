<?php 
/**
 * @name 微信支付申请退款回调接口 
 * @author crazymus < QQ:291445576 >
 * @licenced apache2.0
 * @updated 2016-01-19
 */
 
namespace Org\Util;
interface WechatPayRefund{
	/**
	 * @name 申请退款成功后 需要完成的业务逻辑 
	 * @param 申请退款成功返回的信息，参见https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_4
	 成功的返回值 
	 * @return boolean 
	 */
	public function success($array);
}

?>