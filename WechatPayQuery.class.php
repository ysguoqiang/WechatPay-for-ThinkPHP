<?php 
/**
 * @name 微信支付PHPSDK查询接口 
 * @author crazymus < QQ:291445576 >
 * @licenced apache2.0
 * @updated 2016-01-04
 */
 
namespace Org\Util;
interface WechatPayQuery{
	/**
	 * @name 主动查询支付成功的结果后 需要完成的业务逻辑 
	 * @param 微信传递的支付信息，参见https://pay.weixin.qq.com/wiki/doc/api/app.php?chapter=9_2&index=4
	 执行成功后的返回值 
	 * @return boolean 
	 */
	public function success($array);
}

?>