<?php 
/**
 * @name 微信支付PHPSDK回调接口 
 * @author crazymus < QQ:291445576 >
 * @licenced apache2.0
 * @updated 2016-01-02
 */
 
namespace Org\Util;
interface WechatPayNotify{
	/**
	 * @name 接收到支付成功的结果后 需要完成的业务逻辑 
	 * @param 微信传递的支付信息，参见https://pay.weixin.qq.com/wiki/doc/api/app.php?chapter=9_7&index=3
	 传递的参数 
	 * @return boolean 
	 */
	public function success($array);
}

?>